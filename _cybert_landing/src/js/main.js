import '../local_modules/owl.carousel/dist/owl.carousel.min.js'
// import html2canvas from '../local_modules/html2canvas/dist/html2canvas.min'
import '../local_modules/jquery-modal/jquery.modal.min.js'
// import '../local_modules/jquery-lazy/jquery.lazy.min'
// import './jquery.maskedinput.js'

$(document).ready(() => {
    /*
        ============================================================
        Accordion
        ============================================================
    */
    $(".accordion-item.active").find(".accordion-item__panel").slideDown();
    $(".js-accordion").click(function() {

        var accordionItem = $(this).parent();
        var isActive = accordionItem.hasClass('active');
        var accordionItems = accordionItem.parent().find('.accordion-item');

        closeAccordionItems(accordionItems);

        if(!isActive) {
            var accordionItemContent = $(this).next();
            accordionItemContent.slideDown();

            accordionItem.addClass('active');
        }
    });
    function closeAccordionItems(items) {
        for(var i = 0; i < items.length; i++) {
            var item = $( items[i] );

            var content = item.find('.accordion-item__panel');
            content.slideUp();

            item.removeClass('active');
        }
    }
    /*
        ============================================================
        Gallery
        ============================================================
    */
    $('.js-gallery-carousel').owlCarousel({
        loop: true,
        nav: true,
        navText: [$('.gallery-carousel__nav-prev'),$('.gallery-carousel__nav-next')],
        items: 1,
        responsive:{
            0: {
            },
        }
    })

    /*
        ============================================================
        Modal
        ============================================================
    */
    $(".js-modal-open").click(function(event) {
        event.preventDefault();
        $("#modal-form").modal();
    });
    /*
        ============================================================
        Ajax counter
        ============================================================
    */
    $.ajax({
        url: 'https://1kz.site/bitrix/rid13/counter.php',
        method: 'get',
        dataType: 'json', /* Тип данных в ответе (xml, json, script, html). */
        success(data) {
            console.log(data)
            // {
            //     first_level: 274
            //     second_level: 168
            //     third_level: 191
            // }
            $('#js-engaged-1').text(data.first_level)
            $('#js-engaged-2').text(data.second_level)
            $('#js-engaged-3').text(data.third_level)

            $('#js-budget-1').text(1000000 - data.first_level * 500 + ' ₽')
            $('#js-budget-2').text(750000 - data.second_level * 500 + ' ₽')
            $('#js-budget-3').text(500000 - data.third_level * 500 + ' ₽')
        }
    });
})
