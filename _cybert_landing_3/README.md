# CyberT Landing

## 1. Install NodeJS v14.x.x 
https://nodejs.org/en/blog/release/v14.17.3/

## 2. Install packages
```
npm install
```

## 3. Run development mode
```
npm start
```

## 4. Compile files for production into 'build/' folder
```
npm run build
```
