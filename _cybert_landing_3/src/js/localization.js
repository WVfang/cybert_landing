// The locale our app first shows
const locales = ["ru", "kk"]
let defaultLocale = "ru"
// The active locale
let locale
// Gets filled with active locale translations
let translations = {}
// When the page content is ready...
document.addEventListener("DOMContentLoaded", () => {
    const browserLocales = navigator.languages
    for (let i = 0; i < browserLocales.length; i++) {
        if (locales.includes(browserLocales[i])) {
            defaultLocale = browserLocales[i]
            break
        }
    }

    // Translate the page to the default locale
    setLocale(defaultLocale)

    bindLocaleSwitcher(defaultLocale)
})
// Load translations for the given locale and translate
// the page to this locale
async function setLocale(newLocale) {
    if (newLocale === locale) return
    const newTranslations =
        await fetchTranslationsFor(newLocale)
    locale = newLocale
    document.querySelector('html').lang = locale
    translations = newTranslations
    translatePage()
}
// Retrieve translations JSON object for the given
// locale over the network
async function fetchTranslationsFor(newLocale) {
    // const response = await fetch(`/2/lang/${newLocale}.json`)
    const response = await fetch(`../lang/${newLocale}.json`)
    return await response.json()
}
// Replace the inner text of each element that has a
// data-i18n-key attribute with the translation corresponding
// to its data-i18n-key
function translatePage() {
    document
        .querySelectorAll("[data-i18n-key]")
        .forEach(translateElement)

    document
        .querySelectorAll("[data-i18n-key-placeholder]")
        .forEach((element) => {
            const key = element.getAttribute("data-i18n-key-placeholder")
            const translation = byString(translations, key)
            element.placeholder = translation
        })

    document
        .querySelectorAll("[data-i18n-key-image]")
        .forEach((element) => {
            const key = element.getAttribute("data-i18n-key-image")
            const translation = byString(translations, key)
            if (translation) {
                element.src = translation
            }
        })
}
// Replace the inner text of the given HTML element
// with the translation in the active locale,
// corresponding to the element's data-i18n-key
function translateElement(element) {
    const key = element.getAttribute("data-i18n-key")
    const translation = byString(translations, key)
    element.innerHTML = translation
}

function byString(obj, string) {
    string = string.replace(/\[(\w+)\]/g, '.$1') // convert indexes to properties
    string = string.replace(/^\./, '')  // strip a leading dot
    const stringArr = string.split('.')
    for (let i = 0; i < stringArr.length; ++i) {
        const stringArrPart = stringArr[i]
        if (stringArrPart in obj) {
            obj = obj[stringArrPart]
        } else {
            return
        }
    }
    return obj
}

function bindLocaleSwitcher(initialValue) {
    const switcher =
        document.querySelector("[data-i18n-switcher]")
    switcher.value = initialValue
    switcher.onchange = (e) => {
        // Set the locale to the selected option[value]
        setLocale(e.target.value)
    };
}
